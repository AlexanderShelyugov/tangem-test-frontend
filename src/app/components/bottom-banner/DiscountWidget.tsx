'use client'
import { useRef } from 'react'

import '@styles/bottom-banner.css'
import '@styles/smooth-visibility.css'
import DiscountCode from '@components/discount-code/DiscountCode'
import CloseButton from '@components/close-button/CloseButton'
import openGitlabHandler from '@hooks/AccessGithub'

export default function DiscountWidget() {
    let widgetRef = useRef<HTMLDivElement>(null)
    let onClose = () => {
        widgetRef.current?.classList.add('smooth-hidden')
        localStorage?.setItem("discount-banner-is-closed", "yes");
    }

    return (
        <div className="discount-widget" ref={widgetRef}>
            <div className="discount-widget-layout">
                <CloseButton onClick={onClose} />
                <h1>Black Friday</h1>
                <h2>10%OFF</h2>
                <h3>Use code <DiscountCode /> at checkout</h3>
                <button onClick={openGitlabHandler} />
            </div>
        </div>
    )
}