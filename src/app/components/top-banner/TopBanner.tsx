import '@styles/top-banner.css'
import { isMobile } from 'react-device-detect';
import CloseButton from '@components/close-button/CloseButton';
import openGitlabHandler from '@hooks/AccessGithub'
import DiscountCode from '@components/discount-code/DiscountCode';

const TopBanner = () => {
    return (
        <header className="top-banner">
            <DiscountMenuPortrait />
            {isMobile
                ? <DiscountMenuMobileLandscape />
                : <DiscountMenuDesktopLandscape />
            }
        </header>
    )
}

const DiscountMenuPortrait = () => {
    return (
        <div className="discount-menu-frame portrait">
            <div className="discount-menu-texts">
                <span className="black-friday-title">Black Friday,</span>
                <span className="discount-amount">10%OFF</span>
            </div>
            <a className='discount-menu-arrow' href="" onClick={openGitlabHandler}><img src="menu-icon.svg" alt="Menu Icon for discount" /></a>
        </div>
    )
}

const DiscountMenuMobileLandscape = () => {
    return (
        <div className="discount-menu-frame landscape">
            <div className="discount-menu-texts">
                <span className="black-friday-title">Black Friday</span>
                <div className="divider" />
                <span className="discount-amount">10%OFF</span>
                <div className="divider" />
                <span>Use code <DiscountCode /></span>
            </div>
            <button className='cta' onClick={openGitlabHandler}>Shop now</button>
        </div>
    )
}

const DiscountMenuDesktopLandscape = () => {
    return (
        <div className="discount-menu-frame landscape">
            <div className="discount-menu-texts">
                <span className="black-friday-title">Black Friday, 24-27 Nov</span>
                <div className="divider" />
                <span className="discount-amount">10%OFF</span>
                <div className="divider" />
                <span>Use code <DiscountCode /> at checkout</span>
            </div>
            <button className='cta' onClick={openGitlabHandler}>Shop now</button>
            <CloseButton />
        </div>
    )
}

export default TopBanner