import '@styles/close-button.css'

export default function CloseButton({ ...props }) {
    return (
        <img className='close-button' src="close.svg" alt="Close button" {...props} />
    )
}