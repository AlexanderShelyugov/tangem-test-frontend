import '@styles/discount-code.css'
import { useSnackbar } from 'notistack';

export default function DiscountCode() {
    const discountCode = '10FRIDAY'
    const copyToClipboard = () => {
        navigator.clipboard.writeText(discountCode)
        enqueueSnackbar('Code copied!');
    }
    const { enqueueSnackbar } = useSnackbar();

    return (
        <span className="discount-code" onClick={copyToClipboard}>
            {discountCode}
        </span>
    )
}