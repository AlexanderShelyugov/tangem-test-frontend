'use client'

import { useEffect, useRef, useState } from 'react';
import { InView } from 'react-intersection-observer';

import '@styles/index.css'
import TopBanner from '@components/top-banner/TopBanner';
import DiscountWidget from '@components/bottom-banner/DiscountWidget';
import useLocalStorage from '@hooks/LocalStorageHook';
import { SnackbarProvider } from 'notistack';

const HomePage = () => {
  // TODO move the mediation logic to a separate component
  const ref = useRef<HTMLDivElement>(null)
  const [isClient, setIsClient] = useState(false)
  const [hideDiscountBanner, setHideDiscountBanner] = useLocalStorage("discount-banner-is-closed", "")
  useEffect(() => {
    setIsClient(true)
  }, [])

  return (
    <div className="main-canvas">
      <SnackbarProvider>

        <InView as="div" onChange={(topBannerIsVisible) => {
          let allClasses = ref.current?.classList
          if (allClasses) {
            let classToAdd = topBannerIsVisible ? 'smooth-hidden' : 'smooth-shown'
            let classToRemove = topBannerIsVisible ? 'smooth-shown' : 'smooth-hidden'

            allClasses.remove('invisible')
            allClasses.add(classToAdd)
            allClasses.remove(classToRemove)
          }
        }}>
          <TopBanner />
        </InView>

        {isClient && hideDiscountBanner === 'yes'
          ? null
          : < div ref={ref} className='invisible'>
            <DiscountWidget />
          </div>
        }
      </SnackbarProvider>
    </div >
  )
}

export default HomePage
