export const openGitlabRepo = () => {
    if (typeof window !== 'undefined') {
        window.open('https://gitlab.com/AlexanderShelyugov/tangem-test-frontend', '_blank');
    }
}

const openGitlabHandler = (event: React.MouseEvent<HTMLElement>) => {
    event.preventDefault();
    openGitlabRepo()
}

export default openGitlabHandler