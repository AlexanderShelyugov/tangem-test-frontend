# The test task special 4 Tangem

Done with [Next.js](https://nextjs.org) + [React](https://react.dev). Implemented by [Alexander Shelyugov](https://www.linkedin.com/in/alexander-shelyugov).

## Getting Started

You can access the website [here](https://tangem-test-frontend.vercel.app)

Or you can run it locally with

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

I hope you like it!